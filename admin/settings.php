<?php include"header.php"; ?>

<?php include"../class/admin.php"; ?>
<?php 
    $message=null;
    if(isset($_POST['save_settings'])){
        $site_title = $_POST['site_title'];
        $footer_text = $_POST['footer_text'];
        $sql = "UPDATE settings SET setting_data='$site_title' WHERE setting_id='site_title'";
        
        if($db->query($sql)){
            //
        }else{
            //
        }
        
        $sql = "UPDATE settings SET setting_data='$footer_text' WHERE setting_id='footer_text'";
        if($db->query($sql)){
            //
        }else{
            //
        }
        
        // image file upload code
        $upload_file = $_FILES['header_logo']['tmp_name'];
        $file_name = $_FILES['header_logo']['name'];
        if(is_uploaded_file($upload_file)){
            move_uploaded_file($upload_file, './../uploads/'.$file_name);
            $sql = "UPDATE settings SET setting_data='$file_name' WHERE setting_id='header_logo'";
            if($db->query($sql)){
                //
            }else{
                //
            }
        }
        
    }
    

?>
<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Form elements</a> <a href="#" class="current">Validation</a> </div>
    <h1><i class="icon icon-cog"></i> <span>settings</span></h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Form validation</h5>
          </div>
          <div class="widget-content nopadding">
           
           <!--File upload form
           enctype="multipart/form-data"-->
            <form class="form-horizontal" method="post" action="settings.php" enctype="multipart/form-data">
             
              <div class="control-group">
                <label class="control-label">Website Title</label>
                <div class="controls">
                  <input type="text" name="site_title" id="required" value="<?php echo setting_data('site_title'); ?>">
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label">Footer Text</label>
                <div class="controls">
                  <input type="text" name="footer_text" id="site_title" value="<?php echo setting_data('footer_text'); ?>">
                </div>
              </div>
              <!--File uplad code-->
              <div class="control-group">
                <label class="control-label">Wesite Logo</label>
                <div class="controls">
                 
                  <input type="file" name="header_logo" id="header_logo">
                   
                    <img src="../uploads/<?php echo setting_data('header_logo'); ?>" alt="" class="img-responsive">
                </div>
              </div>
              
              <div class="form-actions">
                <input name="save_settings" type="submit" value="Save Settings" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include"footer.php"; ?>