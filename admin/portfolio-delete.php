<?php
include "auth.php";
$db = new db();
if(isset($_GET['id']) and is_numeric($_GET['id'])){
    $id = $_GET['id'];
    $sql = "DELETE FROM portfolio WHERE id=$id";
    if($db->query($sql)){
        $_SESSION['MSG_SUCCESS'] ='Portfolio deleted successfully';
    }else{
        $_SESSION['MSG_ERROR'] ='Something Wrong please try again';
    }
    header('Location: portfolio_list.php');
}