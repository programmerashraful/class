<?php include"header.php"; ?>
<?php include"../class/admin.php"; ?>
<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">ALL PORTFOLIO</a> <a href="#" class="current">Validation</a> </div>
    <h1><i class="icon icon-cog"></i> <span> PORTFOLIO LIST  </span></h1>
    
    <?php if(isset($_SESSION['MSG_SUCCESS']) or isset($_SESSION['MSG_ERROR'])){  ?>
    
    <div class="alert alert-<?php if(isset($_SESSION['MSG_SUCCESS'])){echo 'success'; }else{echo 'danger';} ?> alert-dismissible fade in">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <?php if(isset($_SESSION['MSG_SUCCESS'])){
            echo $_SESSION['MSG_SUCCESS'];
        }else{
            echo $_SESSION['MSG_ERROR'];
        } ?>              
      </div>
<?php unset($_SESSION["MSG_SUCCESS"]);  unset($_SESSION["MSG_ERROR"]); } ?>
    
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5> Porfolios </h5>
            <a href="portfolio-add.php" class="btn btn-success align-right">Add new portfolio</a>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-borderd">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                    $sql = "SELECT * FROM portfolio ORDER BY id DESC";
                    $result =  $db->query($sql);
                    if($result){
                       while($post = $result->fetch_assoc()){
                    ?>
                    <tr>
                        <td><?php echo $post['id']; ?></td>
                        <td><?php echo $post['post_title']; ?></td>
                        <td><?php echo $post['post_time']; ?></td>
                        <td><img src="../uploads/portfolio/<?php echo $post['post_thumbnail'];  ?>" alt="" class="img-responsive"></td>
                        <td>
                            <div class="btn-group">
                                <a href="portfolio-edit.php?id=<?php echo  $post['id']; ?>" class="btn btn-success">Edit</a>
                                <a href="portfolio-delete.php?id=<?php  echo $post['id']; ?>" onclick="return confirm('DO YOU WANT TO DELETE THIS PSOT')" class="btn btn-danger">Delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php  } } ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include"footer.php"; ?>