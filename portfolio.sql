-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2018 at 12:07 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(255) NOT NULL,
  `post_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_agency` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `post_details` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `post_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_category` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `post_title`, `post_agency`, `post_thumbnail`, `icon`, `post_details`, `post_time`, `post_category`, `color`) VALUES
(3, '2 bedroom house for rent in Dubai', 'Vision Agency', 'post_img.jpg', 'bus-alt', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'purple'),
(4, 'Portfolio Title fouor', 'Vision Agency', 'post_img.jpg', 'home', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'green'),
(5, 'Portfolio Title Five', 'Vision Agency', 'post_img.jpg', 'home', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'green'),
(6, 'Portfolio Title six', 'Vision Agency', 'post_img.jpg', 'music', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'orange'),
(7, 'Portfolio Title Seven', 'Vision Agency', 'post_img.jpg', 'bus-alt', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'purple'),
(8, 'Portfolio Title Eight', 'Last Agency', 'post_img.jpg', 'home', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Enim similique dolores optio consequuntur aut est id iure voluptas iusto, soluta?', '2018-12-13 15:30:44', 'Reality', 'green');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(255) NOT NULL,
  `setting_id` varchar(255) DEFAULT NULL,
  `setting_data` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_id`, `setting_data`) VALUES
(1, 'header_logo', 'header-main-log-logo.png'),
(2, 'site_title', 'PHP WEB APPLICATION'),
(3, 'footer_text', ' ictsylhet.com 2018');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `email`) VALUES
(1, 'Frjana', 'Akther', 'farjana', '202cb962ac59075b964b07152d234b70', 'farjana@gmail.com'),
(2, 'Marjana', 'Ar Rashid', 'marjana', '81b073de9370ea873f548e31b8adc081', 'marjana@gmail.com'),
(3, 'Admin', 'Admin', 'admin', '202cb962ac59075b964b07152d234b70', 'admin@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
