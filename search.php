	 <!--Search section-->
	<section class="search_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 offset-sm-3 search_bar">
					<h1 class="text-center">What are you looking for?</h1>
					<form action="">
						<div class="row">
							<div class="col-sm-6">
								<div class="select_overlay">
									<select class="form-control">
										<option value="">Dubai</option>
										<option value="">Option one</option>
									</select>
									<i class="fas fa-angle-down"></i>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="select_overlay">
									<select class="form-control">
										<option value="">Education</option>
										<option value="">Option one</option>
									</select>
									<i class="fas fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<input type="text" name="" id="" class="form-control" placeholder="Keyword, name, date, ..."/>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-center">
								<input type="submit" value="SEARCH" class="btn btn-danger search_button" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section><!--End of search section-->