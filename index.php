<?php include"header.php"; ?>
<?php include"search.php"; ?>
	
	<!--Filter section-->
	<section class="filter_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="overflow_hidden">
						<ul class="left_filter_menu">
							<li class="active"><a href="">Recommended</a></li>
							<li ><a href="">latest</a></li>
							<li ><a href="">Highlights</a></li>         
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<form action="">
						<div class="row filter_inputs">
							<div class="col-sm-6">
								<div class="input-group">
									<div class="select_overlay">
										<select class="form-control">
											<option value="">Dubai</option>
											<option value="">Option one</option>
										</select>
										<i class="fas fa-angle-down"></i>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="input-group">
									<div class="select_overlay">
										<input type="text" name="" placeholder="Show map" class="form-control" />
										<i class="fas fa-map-marker-alt"></i>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-4">
					<ul class="social_menu pull-right">
						<li><a href="">Filter: </a></li>
						<li><a href="" class="bg_green"><i class="fa fa-home"></i></a></li>
						<li><a href=""><i class="fa fa-user"></i></a></li>
						<li><a href=""><i class="fas fa-graduation-cap"></i></a></li>
						<li><a href="" class="bg_orange"><i class="fas fa-music"></i></a></li>
						<li><a href="" class="bg_purple"><i class="fas fa-bus-alt"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--End of filter section-->
	
	
	<section class="posts_section">
		<div class="container">
		
		    <div class="row">
			    <?php echo home_posts(); ?>
		    </div>
			<!--wide posts-->
			<div class="row">
				<div class="col-sm-6">
					<div class="single_post wide_post green">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/wide-post-image.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
							<div class="media_heading">
								<h1>Vision agency
									<span class="rattings text-center">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="far fa-star"></i>
										<i>(5009)</i>
									</span>
								</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<img src="images/profile-image.png" alt="" class="img-thumbnail img-circle" style="border-radius:50%"/>
							</div>
							<div class="col-sm-9">
								<div class="rattings text-center">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="far fa-star"></i>
									<i>(5009)</i>
								</div>
								<h4>Peter145</h4>
								<p>
								“Aliquam quis pulvinar purus. Etiam cursus ipsum quis enim faucibus, quis posuere orci ornare. Duis mattis sagittis fringilla.” 
								<a href="" class="btn btn-link btn-sm">...read more</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="single_post wide_post orange">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/wide-post-image.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
							<div class="media_heading">
								<h1>Vision agency
									<span class="rattings text-center">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="far fa-star"></i>
										<i>(5009)</i>
									</span>
								</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<img src="images/profile-image.png" alt="" class="img-thumbnail img-circle" style="border-radius:50%"/>
							</div>
							<div class="col-sm-9">
								<div class="rattings text-center">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="far fa-star"></i>
									<i>(5009)</i>
								</div>
								<h4>Peter145</h4>
								<p>
								“Aliquam quis pulvinar purus. Etiam cursus ipsum quis enim faucibus, quis posuere orci ornare. Duis mattis sagittis fringilla.” 
								<a href="" class="btn btn-link btn-sm">...read more</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			
			
			<div class="row">
				<div class="col-sm-3">
					<div class="single_post green">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="single_post orange">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fas fa-music"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="single_post purple">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fas fa-bus-alt"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="single_post green">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-sm-6">
					<div class="single_post wide_post orange">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/wide-post-image.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
							<div class="media_heading">
								<h1>Vision agency
									<span class="rattings text-center">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="far fa-star"></i>
										<i>(5009)</i>
									</span>
								</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<img src="images/profile-image.png" alt="" class="img-thumbnail img-circle" style="border-radius:50%"/>
							</div>
							<div class="col-sm-9">
								<div class="rattings text-center">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="far fa-star"></i>
									<i>(5009)</i>
								</div>
								<h4>Peter145</h4>
								<p>
								“Aliquam quis pulvinar purus. Etiam cursus ipsum quis enim faucibus, quis posuere orci ornare. Duis mattis sagittis fringilla.” 
								<a href="" class="btn btn-link btn-sm">...read more</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="single_post purple">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fas fa-bus-alt"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="single_post green">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="images/post_img.jpg" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-home"></i>
							</span>
							
							<div class="hover_box">
								<a href="#" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>2 bedroom house for rent in Dubai</h6>
							<p>Vision Agency</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<h3 class="text-center"><a href="" class="btn btn-link">Load More...</a></h3>
					<hr />
				</div>
			</div>
		</div>
	</section>
	
	<section class="brand_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center brand_title">
					<h1>What can DuHoot offer to you?</h1>
					<p>Etiam cursus ipsum quis enim faucibus</p>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-10 offset-sm-1">
					<ul class="brand_menu">
						<li><a href=""> <i class="fa fa-home"></i> </a></li>
						<li><a href=""> <i class="fa fa-user"></i> </a></li>
						<li><a href=""> <i class="fas fa-graduation-cap"></i> </a></li>
						<li><a href=""> <i class="fas fa-music"></i> </a></li>
						<li><a href=""> <i class="fas fa-bus-alt"></i> </a></li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 text-center service_title">
					<h1>Reality</h1>
				</div>
			</div>
			
			<div class="row services">
				<div class="col-sm-10 offset-sm-1">
					<div class="row">
						<div class="col-sm-6 single_service">
							<div class="row">
								<div class="col-sm-2 service_icon">
									<i class="fas fa-bolt"></i>
								</div>
								<div class="col-sm-10">
									<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi gravida tellus in purus pharetra, vel dignissim mauris viverra. Maecenas vitae risus est. In hendrerit, massa at.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 single_service">
							<div class="row">
								<div class="col-sm-2 service_icon">
									<i class="fas fa-leaf"></i>
								</div>
								<div class="col-sm-10">
									<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi gravida tellus in purus pharetra, vel dignissim mauris viverra. Maecenas vitae risus est. In hendrerit, massa at.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include"footer.php"; ?>