<?php
$db =  new db();
// this function for reuturn settings data 
function setting_data($setting_id){
    global $db;
    $sql ="SELECT * FROM settings WHERE setting_id='$setting_id'";
    $result = $db->query($sql);
    $data =null;
    if($result){
        while($row = $result->fetch_assoc()){
            $data=$row['setting_data'];    
        }
    }
    return $data;
}

function home_posts(){
    global $db;
    $sql = "SELECT * FROM portfolio ORDER BY id DESC LIMIT 4";
    $result = $db->query($sql);
    $data=null;
    if($result){
        while($row = $result->fetch_assoc()){
            $data.='
				<div class="col-sm-3">
					<div class="single_post '.$row['color'].'">
						<div class="media">
							<span class="pricing">$2,9990</span>
							<img src="./uploads/portfolio/'.$row['post_thumbnail'].'" alt="" class="img-fluid" style="width:100%" />
							<span class="post_icon">
								<i class="fa fa-'.$row['icon'].'"></i>
							</span>
							
							<div class="hover_box">
								<a href="single-portfolio.php?id='.$row['id'].'" class="btn btn-default"> <i class="fas fa-search"></i> Show More</a>
								<p>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="far fa-star"></i>
									<i class="fas fa-comments"></i>
								</p>
							</div>
						</div>
						<div class="description">
							<h6>'.$row['post_title'].'</h6>
							<p>'.$row['post_agency'].'</p>
							<div class="rattings text-center">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="far fa-star"></i>
								<i>(5009)</i>
							</div>
						</div>
					</div>
				</div>
            ';
        }
    }
    return $data;
}