<?php
session_start();
/*
    # This class for connect database 
    # query table data 
*/
class db{
    
    //construct function will run autometically
    public function __construct(){
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->dbname = "portfolio";
        $this->db = $this->db_connect();
        
    }
    
    // this method for connect db
    public function db_connect(){
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if($conn->connect_error){
            echo '<h1>DB connection error</h1>';
            exit();
        }
        return $conn;
    }
    
    //db query methods
    public function query($sql){
        return $this->db->query($sql);
    }
    
    //construct function will run autometically
    public function __destruct(){
        $this->db->close();
    }
    
}