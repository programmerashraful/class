<?php
include"config/db.php";
include"class/frontend.php";
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title><?php echo setting_data('site_title'); ?></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="all" />
	<!--Gotham Font css-->
	<link rel="stylesheet" type="text/css" href="fonts/GothamBook/font.css" media="all" />
	
	<!---fontawesome css -->
	<link rel="stylesheet" type="text/css" href="fonts/fontawesome/css/all.min.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/main.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all" />
</head>
<body>
	
<div class="all_content">
	<!--Header section -->
	<section class="header_section">
		<header>
			<!-- Navigation -->
			<nav class="navbar navbar-expand-lg navbar-default  static-top">
			  <div class="container">
				<a class="navbar-brand" href="index.php">
					  <img src="uploads/<?php echo setting_data('header_logo'); ?>" alt="">
					</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
					  <i class="fas fa-bars"></i>
					</button>
				<div class="collapse navbar-collapse" id="navbarResponsive">
				  <ul class="navbar-nav ml-auto">
					<li class="nav-item active">
					  <a class="nav-link" href="#">Reality
							<span class="sr-only">(current)</span>
						  </a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#">Living</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#">EduCation</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#">Entertainment</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#">Mobility</a>
					</li>
					<li class="nav-item Search_icon">
					  <a class="nav-link" href="#"><i class="fas fa-search"></i></a>
					</li>
					<li class="nav-item login_item">
					  <a class="nav-link" href="#">  <i class="fa fa-user"></i> LOGIN</a>
					</li>
				  </ul>
				</div>
			  </div>
			</nav>
		</header>
	</section> <!--End header section-->